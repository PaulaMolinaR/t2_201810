package model.data_structurs.test;

import java.util.ListIterator;

import junit.framework.TestCase;
import model.data_structures.DoublyLinkedList;

public class DoublyLinkedListTest extends TestCase
{
	//------------------------------------------------
	//Atributes
	//------------------------------------------------
	
	private DoublyLinkedList<String> doublyLinkedList;
	
	//------------------------------------------------
	//Methods
	//------------------------------------------------
	
	/**
	 * Escenario 1: Crea una lista vacía.
	 * @throws Exception 
	 */
	
	public void setupEscenario1() throws Exception 
	{
		try 
		{
			doublyLinkedList = new DoublyLinkedList<String>();
		} 
		catch ( Exception e) 
		{
			throw new Exception("No se debería generar el error " + e.getMessage());
		}
	}
		
	/**
	 * Escenario 2: Crea una lista con elementos.
	 * @throws Exception 	
	 */
	public void setupEscenario2() throws Exception 
	{
		try
		{
			doublyLinkedList = new DoublyLinkedList<String>();
			
			String manzana = "Manzana";
			String pera = "Pera";
			String naranja = "Naranja";
			String uva = "Uva";
			String limon = "Limon";
	
			doublyLinkedList.add(manzana);
			doublyLinkedList.add(pera);
			doublyLinkedList.add(naranja);
			doublyLinkedList.add(uva);
			doublyLinkedList.add(limon);
		}
		catch(Exception e)
		{
			throw new Exception("No se debería generar el error " + e.getMessage());
		}
	}
	
	public void testIsEmpty() throws Exception
	{
		// 1
		setupEscenario1();
		assertEquals("El tamaño no corresponde.", true, doublyLinkedList.isEmpty());
			
		// 2
		setupEscenario2();
		assertEquals("El tamaño no corresponde.", false, doublyLinkedList.isEmpty());	
	}
	
	public void testSize() throws Exception
	{
		// 1
		setupEscenario1();
		assertEquals("El tamaño no corresponde.", 0, doublyLinkedList.size());
	
		// 2
		setupEscenario2();
		assertEquals("El tamaño no corresponde.", 5, doublyLinkedList.size());			
	}
	
	public void testAdd() throws Exception
	{
		String mandarina = "mandarina";
		
		// 1
		setupEscenario1();
		doublyLinkedList.add(mandarina);
		assertEquals("No se agrego el elemento", 1, doublyLinkedList.size());
			
		// 2
		setupEscenario2();
		doublyLinkedList.add(mandarina);
		assertEquals("No se agrego el elemento", 6, doublyLinkedList.size());
	}
	
	public void testHasNext() throws Exception
	{
		// 1
		setupEscenario1();
		ListIterator<String> iter = doublyLinkedList.iterator();
		assertEquals("La lista no esta vacia", false, iter.hasNext());
					
		// 2
		setupEscenario2();
		ListIterator<String> iter2 = doublyLinkedList.iterator();
		assertEquals("El elemento actual no tiene un siguiente", true, iter2.hasNext());
	}
	
	public void testHasPrevious() throws Exception
	{
		// 1
		setupEscenario1();
		ListIterator<String> iter = doublyLinkedList.iterator();
		assertEquals("La lista no esta vacia", false, iter.hasPrevious());
							
		// 2
		setupEscenario2();
		ListIterator<String> iter2 = doublyLinkedList.iterator();
		assertEquals("El elemento actual no es el primero de la lista", false, iter2.hasPrevious());
	}
	
	public void testNext() throws Exception
	{									
		// 2
		setupEscenario2();
		ListIterator<String> iter2 = doublyLinkedList.iterator();
		assertEquals("El elemento actual no es el primero de la lista", "Manzana", iter2.next());
	}
	
	public void testPrevious() throws Exception
	{
		// 2
		setupEscenario2();
		ListIterator<String> iter2 = doublyLinkedList.iterator();
		
		String prev = null;
		boolean encontrado = false;
		for(int i = 0; i < doublyLinkedList.size() && !encontrado ; i++)
		{
			String fruta = iter2.next();
			
			if( fruta.equals("Pera"))
			{
				encontrado = true;
				prev = iter2.previous();
			}
		}
		assertEquals("El elemento actual es el primero de la lista", "Pera", prev);
	}
	
	public void testRemove() throws Exception
	{
		// 2
		setupEscenario2();
		ListIterator<String> iter2 = doublyLinkedList.iterator();
		
		boolean encontrado = false;
		for(int i = 0; i < doublyLinkedList.size() && !encontrado ; i++)
		{
			String fruta = iter2.next();
			
			if( fruta.equals("Pera"))
			{
				encontrado = true;
				iter2.remove();
			}
		}
		
		assertEquals("El elemento no se elimino", 4, doublyLinkedList.size());
	}
}

