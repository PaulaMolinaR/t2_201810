package view;

import java.util.ListIterator;

import java.util.Scanner;


import controller.Controller;
import model.data_structures.DoublyLinkedList;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				//Cargar data
				case 1:
					
					//Memoria y tiempo 
					long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					
					//Cargar data
					Controller.loadServices( );

					//Memoria usada
					long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					System.out.println(((memoryAfterCase1 - memoryBeforeCase1) / 1000000.0) + "MB");
					
					break;
				case 2:
					
					System.out.println("Ingrese el nombre de la compa�ia:");
					String companyName = sc.next();
					DoublyLinkedList<Taxi> taxiList = Controller.getTaxisOfCompany(companyName);
					
					System.out.println("Se encontraron "+ taxiList.size() + " elementos");
					
					// Show each taxi in the taxiList 
					ListIterator<Taxi> iter = taxiList.iterator();
					while(iter.hasNext())
					{
						Taxi taxi = iter.next();
						String companyString = taxi.getCompany();
							
						if(companyString.equals(companyName) )
						{
							System.out.println("TaxiID:" + taxi.getTaxiId() + " Company:" + taxi.getCompany());
						}
						
			        	
					}
					break;
					
				case 3:
					System.out.println("Ingrese el identificador de la comunidad");
					int companyId = Integer.parseInt(sc.next());
					DoublyLinkedList<Service> taxiServicesList = Controller.getTaxiServicesToCommunityArea(companyId);
					

					// Show each service in the taxiServicesList 
					
					ListIterator<Service> serviceIterator = taxiServicesList.iterator();
					while(serviceIterator.hasNext())
					{
						Service service = (Service) serviceIterator.next();
						
						String trip_id = service.getTrip_id(); 
						String taxi_id = service.getTaxi_id(); 
						String trip_seconds = service.getTrip_seconds(); 
						String trip_miles = service.getTrip_miles(); 
						String trip_total= service.getTrip_total(); 
						
						System.out.println(" Trip ID:" + trip_id + " Taxi ID:" + taxi_id + " Trip Seconds:" + trip_seconds + " Trip Miles:" + trip_miles + " Trip Total:" + trip_total);
						
					}
					
					System.out.println("Se encontraron " + taxiServicesList.size() + " elementos");

					break;
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 2----------------------");
		System.out.println("1. Cargar un subconjunto de datos de servicios de taxis");
		System.out.println("2. Dar lista de taxis de una compa�ia");
		System.out.println("3. Dar listado de servicios que finalizan en un �rea espec�fica de la ciudad");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
