package model.data_structures;

import java.util.ListIterator;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface ILinkedList<Item> 
{
	/** Evaluate if the queue is empty. 
	 * @return true if the queue is empty. false in other case.
	 */
	public boolean isEmpty();
	
	/**
	 * Numeros de elementos en la lista
	 */
	public int size();
	
	/** 
	 * Add a new element at the end of the list
	 */
	public void add(Item item); 
	
   /**
    * Returns an iterator that iterates over the items in this list.
    * @return an iterator that iterates over the items in this list.
    */
	public ListIterator<Item> iterator();
}
