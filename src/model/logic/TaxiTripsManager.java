package model.logic;

import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ListIterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ITaxiTripsManager;

import model.vo.Taxi;
import model.vo.TaxiService;
import model.vo.Service;
import model.data_structures.DoublyLinkedList;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	//----------------------------------------------------
	//Atributes
	//----------------------------------------------------
	
	private DoublyLinkedList<Taxi> taxisList;
	private DoublyLinkedList<Service> servicesList;

	// TODO
	// Definition of data model 
	/**
	 * Este método carga en una lista doblemente encadenada, la información proporcionada en el archivo.
	 * Data Binding 
	 */
	public void loadServices (String serviceFile) 
	{	
		taxisList = new DoublyLinkedList<Taxi>();
		servicesList = new DoublyLinkedList<Service>();
		
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader( serviceFile ));
	        Gson gson = new GsonBuilder().create();
	        
	       TaxiService[] taxiServices = gson.fromJson(reader, TaxiService[].class);
	        
	        for(int i = 0; i < taxiServices.length; i++)
	        {
	        	TaxiService taxiService = (TaxiService) taxiServices[i];
	        	
	        	Service service = new Service();
	        	
	        	service.setTaxi_id(taxiService.getTaxi_id());
	        	service.setTrip_id(taxiService.getTrip_id());
	        	service.setTrip_seconds(taxiService.getTrip_seconds());
	        	service.setTrip_miles(taxiService.getTrip_miles());
	        	service.setTrip_total(taxiService.getTrip_total());
	        	service.setDropoff_community_area(taxiService.getDropoff_community_area());
	        	
	        	servicesList.add(service);
	        	
	        	Taxi taxi = new Taxi();
	        	
	        	// Solo agrega los taxis con compañía
	        	if(taxiService.getCompany() != null)
	        	{
	        		taxi.setTaxiId(taxiService.getTaxi_id());
		        	taxi.setCompany(taxiService.getCompany());
		        	taxisList.add(taxi);
	        	}	
	        }
	        
		}   
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			
		}
	}

	public DoublyLinkedList<Taxi> getTaxisOfCompany(String company) 
	{
		DoublyLinkedList<Taxi> taxisOfCompany = new DoublyLinkedList<Taxi>();
		ListIterator<Taxi> iter = taxisList.iterator();
		
		while(iter.hasNext())
		{
			Taxi taxi = iter.next();
			String companyString = taxi.getCompany();
				
			if(companyString.equals(company))
			{
				taxisOfCompany.add(taxi);
			}
		}
		
		return taxisOfCompany;
	}

	@Override
	public DoublyLinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) 
	{
		DoublyLinkedList<Service> taxiServicesToCommunityArea = new DoublyLinkedList<Service>();
		ListIterator<Service> iter = servicesList.iterator();
		
		String communityAreaString = Integer.toString(communityArea);
		while(iter.hasNext())
		{
			Service service = iter.next();
			String dropoff_community_area = service.getDropoff_community_area();
			
			if(communityAreaString.equals(dropoff_community_area))
			{
				taxiServicesToCommunityArea.add(service);
			}
		}
		
		return taxiServicesToCommunityArea;
	}
}
