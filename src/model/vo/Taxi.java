package model.vo;

import model.data_structures.DoublyLinkedList;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String taxi_id;
	private String company;
	private DoublyLinkedList<Service> taxisServiceList;
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}
	
	public void setTaxiId(String taxiId) {
		this.taxi_id = taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	public void setCompany(String company) {
		this.company = company;
	}
	
	@Override
	public int compareTo(Taxi o) 
	{
		return 0;
	}

	public DoublyLinkedList<Service> getTaxiServiceList() {
		return taxisServiceList;
	}

	public void setTaxiServiceList(DoublyLinkedList<Service> taxiServiceList) {
		this.taxisServiceList = taxiServiceList;
	}	
}
