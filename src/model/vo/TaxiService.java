package model.vo;

public class TaxiService 
{
	private String company;
	private String trip_id; 
	private String taxi_id; 
	private String trip_seconds; 
	private String trip_miles; 
	private String trip_total; 
	private String dropoff_community_area;
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getTrip_id() {
		return trip_id;
	}
	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}
	public String getTaxi_id() {
		return taxi_id;
	}
	public void setTaxi_id(String taxi_id) {
		this.taxi_id = taxi_id;
	}
	public String getTrip_seconds() {
		return trip_seconds;
	}
	public void setTrip_seconds(String trip_seconds) {
		this.trip_seconds = trip_seconds;
	}
	public String getTrip_miles() {
		return trip_miles;
	}
	public void setTrip_miles(String trip_miles) {
		this.trip_miles = trip_miles;
	}
	public String getTrip_total() {
		return trip_total;
	}
	public void setTrip_total(String trip_total) {
		this.trip_total = trip_total;
	}
	public String getDropoff_community_area() {
		return dropoff_community_area;
	}
	public void setDropoff_community_area(String dropoff_community_area) {
		this.dropoff_community_area = dropoff_community_area;
	}
}
